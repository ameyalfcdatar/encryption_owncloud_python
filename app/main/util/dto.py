from flask_restplus import Namespace, fields
from werkzeug.datastructures import FileStorage
authorizations = {
    'jwt_token': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'x-jwt-assertion'
    }
}


class UserDto:
    api = Namespace('user', description='user related operations')
    user = api.model('user', {
        'email': fields.String(required=True, description='user email address'),
        'username': fields.String(required=True, description='user username'),
        'password': fields.String(required=True, description='user password'),
        'public_id': fields.String(description='user Identifier')
    })


class AuthDto:
    api = Namespace('auth', description='authentication related operations')
    user_auth = api.model('auth_details', {
        'email': fields.String(required=True, description='The email address'),
        'password': fields.String(required=True, description='The user password '),
    })


class DashboardDto:
    api = Namespace('dashboard', description='Dashboard related APIs', authorizations=authorizations)
    user_model = api.model('user_model', {
        'username': fields.String(required=True, description='Username'),
        'password': fields.String(required=True, description='Password'),
    })
    file_model = api.inherit('file_model', user_model, {
        'file_name': fields.String(required=True, description='File Name')
    })