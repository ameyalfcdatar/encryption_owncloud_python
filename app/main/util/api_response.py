API_RESPONSE = {
    200: 'Success',
    201: 'Created',
    400: 'Failed. Bad request',
    401: 'Unauthorized',
    404: 'Not Found',
    409: 'Conflict',
    429: 'Request Limit Exceeded',
    500: 'Internal Server Error',
    501: 'Not Implemented',
    504: 'Gateway Timeout'
}