from functools import wraps
from flask import request
from app.main.service.auth_helper import Auth

import traceback

def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):

        data, status = Auth.get_logged_in_user(request)
        token = data.get('data')

        if not token:
            return data, status

        return f(*args, **kwargs)

    return decorated


def admin_token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):

        data, status = Auth.get_logged_in_user(request)
        token = data.get('data')

        if not token:
            return data, status

        admin = token.get('admin')
        if not admin:
            response_object = {
                'status': 'fail',
                'message': 'admin token required'
            }
            return response_object, 401

        return f(*args, **kwargs)

    return decorated


def handle_error(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except KeyError as ke:
            return {'status': 'fail', 'message': 'Incorrect key '+str(ke)+' in input parameter'}, 400
        except Exception as e:
            error_message = e.args[0]
            status_code = args[1] if len(args) == 2 else 500
            error_traceback = traceback.format_exc()
            return {'status': 'fail', 'message': error_message, 'error_traceback': error_traceback}, status_code
    return decorated
