import io
from werkzeug.datastructures import FileStorage
from flask import request, send_file
from flask_restplus import Resource
from app.main.util.decorator import token_required
from app.main.util.dto import DashboardDto
from app.main.service.dashboard_helper import DashboardHelper
from app.main.util.api_response import API_RESPONSE
from app.main.staging import staging_dir_path

dashboard_api = DashboardDto.api
user_model = DashboardDto.user_model
file_model = DashboardDto.file_model
api_responses = API_RESPONSE

upload_parser = dashboard_api.parser()
upload_parser.add_argument('file', location='files',
                           type=FileStorage, required=True)
upload_parser.add_argument('username', required=True, help="Username")
upload_parser.add_argument('password', required=True, help="Password")
'''
    method      :   upload
    method type :   POST
    response    :   message, status_code
'''


@dashboard_api.route('/upload', endpoint='with-parser')
class Upload(Resource):
    @dashboard_api.doc(parser=upload_parser)
    def post(self):
        args = upload_parser.parse_args()
        file = args['file']
        username = args['username']
        password = args['password']
        resp = DashboardHelper.upload(file=file, username=username, password=password)
        return resp


'''
    method      :   download
    method type :   POST
    response    :   message, status_code
'''


@dashboard_api.route('/download')
class Download(Resource):
    # @token_required
    @dashboard_api.doc(responses=api_responses)
    @dashboard_api.expect(file_model)
    def post(self):
        """Returns all the entities with geohash and status"""
        data = request.json
        resp = DashboardHelper.download(data=data)
        if resp[1] != 200:
            return {'Message': 'Error Occured', 'Status Code': resp[1]}, resp[1]
        staging_file_path = staging_dir_path + '/' + data['file_name']
        return send_file(
            io.BytesIO(resp[0]['bin_data']),
            # mimetype=resp[0]['mime_type'],
            as_attachment=True,
            attachment_filename=data['file_name'])


'''
    method      :   backup
    method type :   POST
    response    :   message, status_code
'''


@dashboard_api.route('/backup')
class Backup(Resource):
    # @token_required
    # @dashboard_api.doc(responses=api_responses, security='jwt_token')
    @dashboard_api.doc(responses=api_responses)
    @dashboard_api.expect(user_model)
    def post(self):
        """Returns all the entities with geohash and status"""
        data = request.json
        return DashboardHelper.get_hash(data=data)


'''
    method      :   get-hash
    method type :   POST
    response    :   hash_code/message, status_code
'''


@dashboard_api.route('/get-hash')
class GetHash(Resource):
    # @token_required
    # @dashboard_api.doc(responses=api_responses, security='jwt_token')
    @dashboard_api.doc(responses=api_responses)
    @dashboard_api.expect(file_model)
    def post(self):
        """Returns all the entities with geohash and status"""
        data = request.json
        resp = DashboardHelper.get_hash(data=data)
        if resp[1] != 200:
            return {'Message': 'Error Occured', 'Status Code': resp[1]}, resp[1]
        return resp
