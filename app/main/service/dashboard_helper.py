import os
import pyAesCrypt
import owncloud
from os import stat
from app.main.staging import staging_dir_path
from app.main.util.decorator import handle_error
import magic
import hashlib
from app.main.model.user import User

BUFFER_SIZE = 64*1024

oc = owncloud.Client('http://localhost/owncloud')
oc.login('atharva', 'atharva')


class DashboardHelper:
    """
    Helper class for Dashboard backend
    """
    '''
        method      :   upload
        method type :   POST
        response    :   message, status_code
    '''

    @staticmethod
    @handle_error
    def upload(file, username, password):
        user_flag = DashboardHelper.verify_user(username=username, password=password)
        if not user_flag:
            return {'Message': 'Invalid credentials', 'Status Code': 401}, 401
        DashboardHelper.encrypt(file, password, BUFFER_SIZE)
        try:
            oc.mkdir(username)
        except owncloud.owncloud.HTTPResponseError:
            pass
        upload_flag = oc.put_file(username+'/'+file.filename+'.aes', staging_dir_path+'/'+file.filename+'.aes')
        DashboardHelper.clear_staging()
        if upload_flag:
            return {'Message': 'Successfully uploaded', 'Status Code': 200}, 200
        else:
            return {'Message': 'Failed', 'Status Code': 400}, 400

    '''
        method      :   download
        method type :   POST
        response    :   message, status_code
    '''

    @staticmethod
    @handle_error
    def download(data):
        file_name = data['file_name']
        username = data['username']
        password = data['password']
        user_flag = DashboardHelper.verify_user(username=username, password=password)
        if not user_flag:
            return {'Message': 'Invalid credentials', 'Status Code': 401}, 401
        oc_file_path = username+'/'+file_name+'.aes'
        staging_file_path = staging_dir_path+'/'+file_name+'.aes'
        try:
            oc.get_file(oc_file_path, staging_file_path)
        except owncloud.owncloud.HTTPResponseError:
            return {'Message': 'File '+file_name+' does not exist on cloud', 'Status Code': 404}, 404

        mime = magic.Magic(mime=True)
        DashboardHelper.decrypt_file(staging_file_path, password, BUFFER_SIZE)
        resp_mime_type = mime.from_file(staging_file_path.split('.aes')[0])
        print('mime type:', resp_mime_type)
        with open(staging_file_path.split('.aes')[0], "rb") as binary_file:
            data = binary_file.read()
        DashboardHelper.clear_staging()
        return {'bin_data': data, 'mime_type': resp_mime_type}, 200

    '''
        method      :   backup
        method type :   POST
        response    :   message, status_code
    '''

    @staticmethod
    @handle_error
    def backup(user_name):
        return {'Message': 'Yet to implement', 'Status Code': 501}


    @staticmethod
    @handle_error
    def encrypt(file, password, buffer_size):
        file_name = file.filename
        file.save(staging_dir_path+'/'+file_name)
        try:
            with open(staging_dir_path+'/'+file_name, "rb") as fIn:
                with open(staging_dir_path+'/'+file_name+'.aes', "wb") as fOut:
                    encrypt_flag = pyAesCrypt.encryptStream(fIn, fOut, password, buffer_size)
        except:
            raise Exception('Error occured')

    @staticmethod
    @handle_error
    def decrypt_file(staging_file_path, password, buffer_size):
        # decrypt
        encFileSize = stat(staging_file_path).st_size
        with open(staging_file_path, "rb") as fIn:
            with open(staging_file_path.split('.aes')[0], "wb") as fOut:
                try:
                    # decrypt file stream
                    stream = pyAesCrypt.decryptStream(fIn, fOut, password, buffer_size, encFileSize)
                except ValueError:
                    return None
        return stream

    @staticmethod
    @handle_error
    def clear_staging():
        for the_file in os.listdir(staging_dir_path):
            file_path = os.path.join(staging_dir_path, the_file)
            try:
                if os.path.isfile(file_path) and file_path.split('/')[-1] != '__init__.py':
                    os.unlink(file_path)
            except Exception as e:
                raise Exception

    @staticmethod
    @handle_error
    def get_hash(data):
        file_name = data['file_name']
        username = data['username']
        password = data['password']
        user_flag = DashboardHelper.verify_user(username=username, password=password)
        if not user_flag:
            return {'Message': 'Invalid credentials', 'Status Code': 401}, 401
        oc_file_path = username + '/' + file_name + '.aes'
        staging_file_path = staging_dir_path + '/' + file_name + '.aes'
        try:
            oc.get_file(oc_file_path, staging_file_path)
        except owncloud.owncloud.HTTPResponseError:
            return {'Message': 'File ' + file_name + ' does not exist on cloud', 'Status Code': 404}, 404

        DashboardHelper.decrypt_file(staging_file_path, password, BUFFER_SIZE)
        decrypted_file_path = staging_file_path.split('.aes')[0]
        sha256_hash = hashlib.sha256()
        with open(decrypted_file_path, "rb") as f:
            # Read and update hash string value in blocks of 4K
            for byte_block in iter(lambda: f.read(4096), b""):
                sha256_hash.update(byte_block)
        DashboardHelper.clear_staging()
        return {'hash': sha256_hash.hexdigest(), 'file_name': data['file_name'], 'status_code': 200}, 200

    @staticmethod
    def verify_user(username, password):
        try:
            # fetch the user data
            user = User.query.filter_by(username=username).first()
            if user and user.check_password(password):
                return True
            else:
                return False
        except:
            return False
