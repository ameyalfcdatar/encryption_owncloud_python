from flask_restplus import Api
from flask import Blueprint

from .main.controller.user_controller import api as user_ns
from .main.controller.auth_controller import api as auth_ns
from .main.controller.dashboard_controller import dashboard_api as dashboard_ns
blueprint = Blueprint('api', __name__)

api = Api(blueprint,
          title='CloudCryptor API documentation',
          version='1.0',
          description='The swagger documentation of the RESTful APIs in CloudCryptor'
          )

api.add_namespace(user_ns, path='/user')
api.add_namespace(auth_ns)
api.add_namespace(dashboard_ns)